seen_fragments = Set({ String, Int32 }).new

STDIN.each_line(chomp: true) do |line|
  line.size.times do |index|
    fragment = "#{line[0...index]}#{line[index + 1..-1]}"

    if seen_fragments.includes?({ fragment, index })
      puts fragment
      exit
    end

    seen_fragments << { fragment, index }
  end
end
