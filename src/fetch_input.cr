require "http/client"

YEAR = 2018
DAY = ARGV.first? || Time.now.day

client = HTTP::Client.new("adventofcode.com", tls: true)
client.before_request do |request|
  request.cookies["session"] = ENV["AOC_SESSION"]
end

print client.get("/#{YEAR}/day/#{DAY}/input").body
