record Square, x : Int32, y : Int32

struct Range(B, E)
  def &(other : self)
    self.class.new(
      { self.begin, other.begin }.max,
      { self.end, other.end }.min,
      self.exclusive? && other.exclusive?
    )
  end

  def each
    return if @exclusive ? @begin >= @end : @begin > @end

    current = @begin
    while current < @end
      yield current
      current = current.succ
    end
    yield current if !@exclusive && current == @end
  end
end

struct Claim
  getter id : Int32, x : Int32, y : Int32, width : Int32, height : Int32

  def initialize(@id, @x, @y, @width, @height)
  end

  def horizontal_side
    (@x...@x + @width)
  end

  def vertical_side
    (@y...@y + @height)
  end

  def self.from_io(io : IO)
    attributes = {
      io.gets(" @ ", chomp: true),
      io.gets(",", chomp: true),
      io.gets(": ", chomp: true),
      io.gets("x", chomp: true),
      io.gets("\n", chomp: true)
    }

    new(*attributes.map(&.not_nil!.to_i))
  end

  def overlaps?(other : Claim)
    (horizontal_side & other.horizontal_side).each do |square_x|
      (vertical_side & other.vertical_side).each do |square_y|
        return true
      end
    end

    return false
  end
end

class ClaimsParser
  include Iterator(Claim)

  def initialize(@io : IO)
  end

  def next
    return stop if @io.read_byte.nil?
    Claim.from_io(@io)
  end
end


claims = ClaimsParser.new(STDIN).to_set

claims.to_a.each_combination(2) do |(claim_a, claim_b)|
  next unless claim_a.overlaps?(claim_b)
  claims.delete(claim_a)
  claims.delete(claim_b)
end

raise "Multiple non-overlapping claims" if claims.size > 1
puts claims.first.id
