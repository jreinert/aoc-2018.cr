seen_frequencies = Set(Int32).new

cycled_frequencies = STDIN.each_line.map(&.to_i).to_a.cycle

result = 0

cycled_frequencies.each do |frequency|
  result += frequency
  break if seen_frequencies.includes?(result)
  seen_frequencies << result
end

puts result
